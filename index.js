

	let firstName = "JESSA";
	let lastName = "DEOCAMPO";
	let myAge = 25;
	let myHobbies = ["Playing guitar", " Swimming", " Photograpy"];
	let workAddress = {

		houseNumber: 12,
		street: 'PUROK BANTAM',
		city: 'BACOLOD City',
		province: 'NEGROS OCC'

	};


	console.log("First Name: " + firstName);
	console.log("Last Name: " + lastName);
	console.log("Age: " + myAge);
	console.log("Hobbies:");
	console.log(myHobbies);
	console.log("Work Address:");
	console.log(workAddress);


	
	let fullName = "Steve Rogers";
	console.log("My full name is " + fullName);

	let currentAge = 40;
	console.log("My current age is: " + currentAge);
	
	let friends = ["Tony","Bruce", "Thor","Natasha","Clint","Nick"];
	console.log("My Friends are: ")
	console.log(friends);

	let profile = {

		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false

	};

	console.log("My Full Profile: ")
	console.log(profile);

	let bestFriend = "Bucky Barnes";
	console.log("My bestfriend is: " + bestFriend);

	const lastLocation = "Arctic Ocean";
	console.log("I was found frozen in: " + lastLocation);

